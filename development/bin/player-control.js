
var levels;
var level;
var currentQuality;
var qualityBtn;

function qualityControl(player) {
  moveTimeslider(player);
	qualityBtn = document.getElementsByClassName("jw-icon-settings jw-settings-submenu-button");
	player.on('levels', function(data) {
		currentQuality = data.currentQuality;
		levels = data.levels;
		level = levels[currentQuality];
		
		//console.log('levels', currentQuality, data, level, level.hlsjsIndex, level.label, level.level_id);
		//if (currentQuality  == 0 && (level.hlsjsIndex == -1 || level.label == "Auto" || level.level_id == "auto")) {
		setStretching(player, level.height);
		checkQuality(player, level.height);
	});
	player.on('levelsChanged', function(data) {
		//console.log('levelsChanged', data);
		levels = data.levels;
		currentQuality = data.currentQuality;
		level = levels[currentQuality];

		checkQuality(player, level.height);
	});
	player.on('visualQuality', function(data) {
		//console.log('visualQuality', data);
		level = data.level;

		setStretching(player, level.height);
		checkQuality(player, level.height);
	});
}

function setStretching(player, height) {
	if (undefined != height && height < 548) {
		player.setConfig({stretching: 'fill'});
	} else {
		player.setConfig({stretching: 'uniform'});
	}
	//console.log('getConfig', height, player.getConfig().stretching);
}

function checkQuality(player, height) {
	//console.log('checkQuality', height);
	if (undefined == height) {
		visualQuality = player.getVisualQuality();
		if (null != visualQuality) {
			if (null != visualQuality.level) {
				checkQuality(player, visualQuality.level.height);
			}
		}
	} else if (height > 2000) {		// 1440px < 4K = 2160px
		showQuality4K();
	} else if (height > 1080) {		// 1080px < 2K < 1440px
		showQuality2K();
	} else if (height < 548) {
		showQualitySD();
	} else {
		resetQuality();
	}
}

function showQuality4K() {
	resetQuality();
	qualityBtn[0].classList.add("jw-icon-4k");
}

function showQuality2K() {
	resetQuality();
	qualityBtn[0].classList.add("jw-icon-2k");
}

function showQualitySD() {
	resetQuality();
	qualityBtn[0].classList.add("jw-icon-sd");
}

function resetQuality() {
	if (null == qualityBtn)
		qualityBtn = document.getElementsByClassName("jw-icon-settings jw-settings-submenu-button");
	qualityBtn[0].classList.remove("jw-icon-sd");
	qualityBtn[0].classList.remove("jw-icon-2k");
	qualityBtn[0].classList.remove("jw-icon-4k");
}

function connectPlayer(player, player2) {
	player.on('play', function() {
		player2.play();
	});
	player.on('pause', function() {
		player2.pause();
	});
	player.on('buffer', function() {
		player2.pause();
	});
	player.on('stop', function() {
		player2.stop();
	});
	player.on('seek', function(data) {		//console.log('seek', data, data.position, data.offset);
		player2.seek(data.offset);
	});
	player.on('complete', function() {
		player2.pause();
	});
	player.on('idle', function() {
		player2.pause();
	});
	player.on('error', function() {
		player2.pause();
	});

	var unmuteId = "volumeButton";    //unmuteButtonWrapper
	var unmuteButton = document.getElementById(unmuteId);
	var toolbarWrapper = document.getElementById("toolbarWrapper");
	player.on('mute', function(data) {		//wplayer().on('volume') + data.volume > 0
		if (!data.mute) {
			if(null!=unmuteButton)unmuteButton.style.display = "none"; 
			toolbarWrapper.className = "ytp-unmute-none ";
		}
	});
	if(null!=unmuteButton)unmuteButton.addEventListener('click', function (event) {
		this.style.display = "none"; 
		player.setMute(false);
		toolbarWrapper.className = "ytp-unmute-none ";
	});
}

function moveTimeslider(player) {
	player.on('ready', function() {
		// Move the timeslider in-line with other controls
		const playerContainer = player.getContainer();
		const buttonContainer = playerContainer.querySelector('.jw-button-container');
		const spacer = buttonContainer.querySelector('.jw-spacer');
		const timeSlider = playerContainer.querySelector('.jw-slider-time');
		buttonContainer.replaceChild(timeSlider, spacer);
	});
}