  jwplayer.key = "rWNl23nkOEzswsr7LKlzCwYeTz8fLdjwPOL0CQoK9f0=";
//jwplayer.key = "1AdUAA/c0CaHFyc5pUwXOfqn2CCv+p33wHVmvLn1MDT6pNimeElIpQ==";
//jwplayer.key = "633e7K0m6b6B10n67xCxa+lRN2posHWNOjXHaopW/MZUcLiA3hMZ1jujNuo=";

let Player, keywords, start_time, ori_video_url_1, ori_video_url_2;

function initJWPlayer(video_url_1, video_url_2, transcription_url) {
  var chapters = [];
  var captions = [];
  var caption = -1;
  var matches = [];
  var blocks = [];
  var queries = [];
  var query = "";
  var cycle = -1;
  var nodeSize = 20;

  var show_keyword = true;
  var b_transcript = false;
  var load_transcript = false;

  var video, video1;
  var jwplayer1, jwplayer2;
  var player, player2;

  var unmuteButtonWrapper;
  var searchInput;
  var countWordWrapper;
  var searchCurrent;
  var searchTotal;
  var backWordButton;
  var nextWordButton;
  var closeWordButton;
  var searchButton;
  var keywordButton;
  var tagsWrapper;
  var tagsList;
  
  var heatmapWrapper = document.getElementById("heatmap-wrapper");
  var heatmapBlocks = document.getElementById("heatmap-blocks");
  var transcript = document.getElementById('transcript');
  var transcriptWrapper;
  var transcriptWrapper2; // This is for multi videos
  
  // Two Video Player
  var playerContainer, secondContainer;

  // Two Video Players Control Button
  //var sideButton, fullButton, swapButton;

  var ready_1 = false;
  var ready_2 = false;

  if (!document.getElementById("player")) {
    return;
  }

  var allContainer = document.getElementsByClassName("all-container")[0];

  //var swapControlButton, resolutionControlButton;
  var castButton        = document.getElementById("stage-cast");
  var maximizeButton    = document.getElementById("stage-maximize");
  var splitSingleButton = document.getElementById("stage-splitSingleButton");
  var swapButton        = document.getElementById("stage-swapButton");

  var offsetTime = 0;
  if (start_time) {
    var hh = Number(start_time.split(':')[0])
    var mm = Number(start_time.split(':')[1])
    var ss = Number(start_time.split(':')[2])
    offsetTime = hh * 3600 + mm * 60 + ss;
    //start_time = null;
  }

  

  if (video_url_1) {
    let id = Number(document.getElementById("meta-videoId").innerHTML);
    const buttonId = 'show-cc-button';
    const iconPath = 'jwplayer-dictatio/svg/cc.svg';
    const tooltipText = 'Show / Hide the Caption and Heatmap.';
    
    let playlist1_config = [{
      file: video_url_1,
      starttime : offsetTime
    }];
    if (video_url_1.indexOf("youtube.com")>0) {
      playlist1_config = [{
        file: video_url_1,
        type: "mp4"
      }];
    }

    jwplayer1 = jwplayer('player');
    jwplayer1.setup({
      width: '100%',
      aspectratio: '16:9',
      base: './jwplayer-8.13.4/',
      playlist: playlist1_config,
      autostart: true,
      mute: true, //optional, but recommended
      stretching: 'uniform',    //uniform, exactfit, fill, none
      playbackRateControls: true,
      playbackRates: [0.25, 0.5, 0.75, 1, 1.25, 1.5, 2],
      skin: {
        controlbar: {
          "icons": "rgba(255,255,255,1.0)",
          "iconsActive": "#1ABC9C"
        },
        timeslider: {
          progress: "#00adef",
          buffer: "#666",
          rail: "rgba(23,35,34,0)"
        },
        menus: {
          "textActive": "#1ABC9C"
        },
        tooltips: {
          "text": "#1ABC9C"
        }
      },
      abouttext: "Dictatio || Video Management Platform",
      aboutlink: "https://dictatio.com",
      ga: {
        label: id
      }
    });
    qualityControl(jwplayer1);
    
    // Call the player's `addButton` API method to add the custom button
    if (transcription_url) {
      jwplayer1.addButton(iconPath, tooltipText, buttonClickAction, buttonId);
    }
    
    // This function is executed when the button is clicked
    function buttonClickAction() {
      // Remove the anchor from the page, it's not needed anymore
      transcript.classList.toggle("active");
      heatmapWrapper.classList.toggle("active");
    }
  }

  if (video_url_2) {
    let playlist2_config = [{
      file: video_url_1,
      starttime : offsetTime
    }];
    if (video_url_2.indexOf("youtube.com")>0) {
      playlist2_config = [{
        file: video_url_2,
        type: "mp4"
      }];
    }
    jwplayer2 = jwplayer('player2');
    jwplayer2.setup({
      width: '100%',
      aspectratio: '16:9',
      playlist: playlist2_config,
      controls: false,
      stretching: 'fill',
      mute: true,
      autostart: true
    });
    jwplayer2.on('ready', function () {
      ready_2 = true;
      checkPlayerReady();
    })
    connectPlayer(jwplayer1, jwplayer2);
  }

  jwplayer1.on('ready', function () {
    ready_1 = true;
    checkPlayerReady();
  })

  jwplayer1.on("error", (err) => {
    try {
      // If transcription is not finished yet
      if ((Number(err.code) >= 230000) && (Number(err.code) < 240000)) {
        initJWPlayer(ori_video_url_1, ori_video_url_2, transcription_url)
      }
    } catch (e) {
      console.log(err);
    }
  })

  function checkPlayerReady() {
    //console.log('ready ', ready_1, ready_2, video_url_2);
    if ((ready_2 || !video_url_2) && ready_1) {
      if (transcription_url) {
        loadCaptions()
      } else if (!transcription_url) {
        //let captionButton = document.getElementById("captionButton");
        //captionButton.classList.add("no-caption")
      }
      setUpControls()
    }
  }

  function setUpControls() {
    //if (!video)
    video = document.getElementsByClassName("jw-video")[0];

    if (video_url_2) {
      //if (video_url_2 && !video1)
        video1 = document.getElementsByClassName("jw-video")[1];
      // This is to sync both videos
      var b = setInterval(function () {
        //console.log("This is for video::: ", jwplayer1.getState(), jwplayer2.getState(), b);
        if (jwplayer1.getState() === 'playing' && jwplayer2.getState() === 'playing') {
          hideLoading();
          clearInterval(b);
        } else if (jwplayer1.getState() === 'playing' && jwplayer2.getState() === 'buffering') {
          jwplayer1.pause();
        } else if (jwplayer1.getState() === 'buffering' && jwplayer2.getState() === 'playing') {
          jwplayer2.pause();
        } else if (jwplayer1.getState() === 'paused' && jwplayer2.getState() === 'paused') {
          jwplayer1.play();
          jwplayer2.play();
          hideLoading();
          clearInterval(b);
        } else if (jwplayer1.getState() === 'paused' && jwplayer2.getState() === 'playing') {
          jwplayer1.play();
          hideLoading();
          clearInterval(b);
        } else if (jwplayer1.getState() === 'playing' && jwplayer2.getState() === 'paused') {
          jwplayer2.play();
          hideLoading();
          clearInterval(b);
        }
      }, 250);
    } else if (!video_url_2) {
      hideLoading();
    }

    function hideLoading() {
      if (!document.getElementById("initializing")) {
        return;
      }
      document.getElementById("initializing").style.display = "none";
    }

    // Set start_time by firstFrame event
    /*if (start_time) {
      jwplayer1.on('firstFrame', () => {
        let hh = Number(start_time.split(':')[0])
        let mm = Number(start_time.split(':')[1])
        let ss = Number(start_time.split(':')[2])
        let offsetTime = hh * 3600 + mm * 60 + ss;
        console.log("firstFrame::: ", start_time, hh, mm, ss, offsetTime);
        jwplayer().seek(offsetTime);
        start_time = null;
      });
    }*/

    // Sliders
    //seekBar = document.getElementById("seek-bar");

    // Caption Button
    //captionButton = document.getElementById("captionButton");
    player = document.getElementById('player');
    player2 = document.getElementById('player2');

    transcriptWrapper = document.getElementsByClassName("transcript-wrapper")[0];

    playerContainer = document.getElementsByClassName("player-container")[0];
    secondContainer = document.getElementsByClassName("second-container")[0];
    transcriptWrapper2 = document.getElementsByClassName("transcript-wrapper-2")[0];

    // Event listener for the cast-tv button
    castButton.addEventListener("click", function () {
      if (castButton.classList.contains("stage-cast")) {
        castButton.classList.remove("stage-cast");
        castButton.classList.add("stage-cast-connected");
        castButton.title = "Disconnect Cast";
      } else {
        castButton.classList.remove("stage-cast-connected");
        castButton.classList.add("stage-cast");
        castButton.title = "Cast to TV";
      }
    });

    // Event listener for the full-screen button
    maximizeButton.addEventListener("click", function () {
      // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
      if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        openFullscreen();
        maximizeMode();
      } else {
        closeFullscreen();
        minimizeMode();
      }
    });

    // Two Video Player
    if (video_url_2) {
      // Event listener for the split-single button
      splitSingleButton.addEventListener("click", function () {
        if (splitSingleButton.classList.contains("stage-fullFrameButton")) {
          fullFrameMode();
        } else {
          sideBySideMode();
        }
      });

      // Event listener for the swap-player button
      swapButton.addEventListener("click", function () {
        if (!video1) {
          return;
        }
        let video_parent = video.parentElement;
        let video1_parent = video1.parentElement;
        video_parent.appendChild(video1);
        video1_parent.appendChild(video);
        video.play();
        if (video1) {
          video1.play();
        }
        if (swapButton.classList.contains("stage-swapLeft")) {
          swapButton.classList.remove("stage-swapLeft");
          swapButton.classList.add("stage-swapRight");
        } else {
          swapButton.classList.remove("stage-swapRight");
          swapButton.classList.add("stage-swapLeft");
        }
      });
    } else {
      splitSingleButton.style.display = "none";
      swapButton.style.display = "none";
    }

    /* Function to open fullscreen mode */
    function openFullscreen() {
      if (allContainer.requestFullscreen) {
        allContainer.requestFullscreen();
      } else if (allContainer.mozRequestFullScreen) {      /* Firefox */
        allContainer.mozRequestFullScreen();
      } else if (allContainer.webkitRequestFullscreen) {  /* Chrome, Safari & Opera */
        allContainer.webkitRequestFullscreen();
      } else if (allContainer.msRequestFullscreen) {      /* IE/Edge */
        allContainer = window.top.document.body           //To break out of frame in IE
        allContainer.msRequestFullscreen();
      }
    }

    /* Function to close fullscreen mode */
    function closeFullscreen() {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) {
        window.top.document.msExitFullscreen();
      }
      //https://stackoverflow.com/questions/7179535/set-window-to-fullscreen-real-fullscreen-f11-functionality-by-javascript
      /*if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }*/
    }

    function maximizeMode() {
      maximizeButton.classList.remove("stage-maximize");
      maximizeButton.classList.add("stage-minimize");
      maximizeButton.title = "Minimize";
    }

    function minimizeMode() {
      maximizeButton.classList.remove("stage-minimize");
      maximizeButton.classList.add("stage-maximize");
      maximizeButton.title = "Maximize";
    }

    function checkFullscreenChange() {
      if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        minimizeMode();
      } else {
        maximizeMode();
      }
    }

    // Events https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_fullscreenchange
    document.addEventListener("fullscreenchange", function() {
      checkFullscreenChange();
    });
    document.addEventListener("mozfullscreenchange", function() {
      checkFullscreenChange();
    });
    document.addEventListener("webkitfullscreenchange", function() {
      checkFullscreenChange();
    });
    document.addEventListener("msfullscreenchange", function() {
      checkFullscreenChange();
    });

    function sideBySideMode() {
      splitSingleButton.title = "Single Screen";
      splitSingleButton.classList.remove("stage-sideBySideButton");
      splitSingleButton.classList.add("stage-fullFrameButton");
      
      //player.classList.remove("left-side");
      //if (video_url_2) {
        playerContainer.classList.add("left-wrapper");
        secondContainer.classList.add('right-wrapper');
        /*secondContainer.style.width = (playerContainer.parentElement.offsetWidth - playerContainer.offsetWidth - 5);
        if (window.innerWidth > 1440) {
          secondContainer.style.width = secondContainer.offsetWidth - 60;
        }*/
        //player2.style.height = player2.offsetWidth * (740 / 1140);
        document.getElementsByClassName("all-container")[0].classList.add("two-video-container");
      //}
      
      b_transcript = false;
      drawControls();
    }

    function fullFrameMode() {
      splitSingleButton.title = "Split Screen"; 
      splitSingleButton.classList.remove("stage-fullFrameButton");
      splitSingleButton.classList.add("stage-sideBySideButton");

      playerContainer.classList.remove("left-wrapper");
      secondContainer.classList.remove("right-wrapper");

      document.getElementsByClassName("all-container")[0].classList.remove("two-video-container");
      drawControls();
    }

    // Unmute button for interactive document
    let unmuteId = "volumeButton";    //unmuteButtonWrapper
    unmuteButtonWrapper = document.getElementById(unmuteId);
    unmuteButtonWrapper.addEventListener("click", function () {
      jwplayer1.setMute(false);
      unmuteButtonWrapper.style.display = "none";
    });
    
    countWordWrapper = document.getElementById("searchCount");
    searchCurrent = document.getElementById("searchCurrent");
    searchTotal = document.getElementById("searchTotal");
    backWordButton = document.getElementById("backWordButton");
    nextWordButton = document.getElementById("nextWordButton");
    closeWordButton = document.getElementById("closeWordButton");
    searchInput = document.getElementById("searchInput");
    searchButton = document.getElementById("searchButton");
    tagsWrapper = document.getElementById("tagsWrapper");
    keywordButton = document.getElementById("keywordButton");

    // Event listener for back word button
    if(null != backWordButton) {
      backWordButton.addEventListener("click", function () {
        if (matches.length === 0) return;
        if (cycle > 0) {
          cycleSearch(cycle - 1);
        } else {
          cycleSearch(matches.length - 1);
        }
      });
    }

    // Event listener for next word button
    if(null != nextWordButton) {
      nextWordButton.addEventListener("click", function () {
        if (matches.length === 0) return;
        if (cycle >= matches.length - 1) {
          cycleSearch(0);
        } else {
          cycleSearch(cycle + 1);
        }
      });
    }

    if(null != searchInput) {
      searchInput.addEventListener('keydown', function (e) {
        if (e.keyCode == 13) {
          let q = this.value;
          searchTranscript(q);
        }
      });
    }

    if(null != searchButton) {
      searchButton.addEventListener("click", function () {
        if (searchInput.classList.contains("active")) {
          if (searchInput.value=="") {
            searchInput.classList.toggle("active");
          } else {
            let q = searchInput.value;    //.toLowerCase()
            searchTranscript(q);
          }
        } else {
          searchInput.classList.toggle("active");
        }
      });
    }

    if(null != closeWordButton) {
      closeWordButton.addEventListener("click", function () {
        searchInput.value = "";
        searchInput.classList.remove('min');
        countWordWrapper.classList.remove('active');
        closeWordButton.classList.remove('active');
        resetSearch();
        checkAllBlocks();
      });
    }

    if(null != keywordButton) {
      keywordButton.addEventListener("click", function () {
        if (tagsWrapper.classList.contains("active")) {
          tagsWrapper.style.display = "all 0.5s ease";    //ease in out in-out 
          tagsWrapper.classList.remove("active");
          setTimeout(function(){
            tagsWrapper.style.display = "none";
          }, 500);
        } else {
          searchInput.classList.add('active');
          tagsWrapper.style.display = "block";
          setTimeout(function(){
            tagsWrapper.style.display = "all 0.5s ease-out";    //in out
            tagsWrapper.classList.add("active");
          }, 50);
        }
        //tagsWrapper.classList.toggle("active");
      });
    }

    tagsList = document.getElementsByClassName("listing__keywords-of-topic");
    if(null != tagsList && tagsList.length > 0) {
      for (var i=0; i < tagsList.length; i++) {
        queries.push(tagsList[i].innerHTML);
        tagsList[i].onclick = function(){
          let q = this.innerHTML;
          q = q.replace(/&amp;/g, '&');
          searchInput.value = q;
          searchTranscript(q);    //.toLowerCase()
        }
      };
    }

    var timer;
    //var intervalIsGoingOn = false;
    let toolbarControls = document.getElementById("toolbarControls");
    toolbarControls.onmouseover = function() {
      //console.log("toolbarControls.onmouseover");
      hiddenToolbar(7000);
    }
    jwplayer1.getContainer().onmouseover = function() {
      //console.log("getContainer.onmouseover");
      toolbarControls.classList.add("active");
      hiddenToolbar();
    }
    jwplayer1.getContainer().onmousemove = function() {
      //console.log("getContainer.onmousemove");
      toolbarControls.classList.add("active");
      hiddenToolbar();
    }
    jwplayer1.getContainer().onmouseout = function() {
      hiddenToolbar();
    };
    jwplayer1.on('pause', function() {
      toolbarControls.classList.add("active");
    });
    jwplayer1.on('idle', function() {
      toolbarControls.classList.add("active");
    });
    jwplayer1.on('play', function() {
      hiddenToolbar();
    });
    if(null != searchInput) {
      searchInput.onfocus = function(){
        //console.log("searchInput.onfocus");
        hiddenToolbar(7000);
      };
      /*searchInput.oninput = function(){
        //console.log("searchInput.oninput");
        hiddenToolbar(7000);
      };*/
    }
    var hiddenToolbar = function (milliseconds) {
      clearTimeout(timer);
      if (undefined == milliseconds) milliseconds = 2000;
      //if(intervalIsGoingOn) return;
      //intervalIsGoingOn = true;
      timer = setTimeout(function() {
        if (jwplayer1.getMute())return;
        if (searchInput.value != "")return;
        toolbarControls.classList.remove("active");
        //intervalIsGoingOn = false;
      }, milliseconds);
    }
    
    // Event listener for the sync time
    /*setInterval(function () {
      try {
        if (!video1) {
          return;
        }
        if (video1.currentTime > video.currentTime + 0.2 || video1.currentTime < video.currentTime - 0.2) {
          video1.currentTime = video.currentTime;
        }
        if (!video.paused && video1.paused) {
          video1.play();
        }
      } catch (err) {
        console.log(err);
      }
    }, 200);*/

    //Event for caption button
    /*captionButton.addEventListener("click", function () {
      setUpCaption();
    })*/

    var setUpCaption = function () {
      if (!transcription_url) {
        //captionButton.classList.add("no-caption")
        return;
      }
      if (!b_transcript) {
        player.classList.add("left-side");

        // This is for two video players
        while (transcriptWrapper2.childNodes.length > 0) {
          transcriptWrapper.appendChild(transcriptWrapper2.childNodes[0]);
        }

        transcriptWrapper.classList.add("right-side");

        // Multi Video
        if (video_url_2) {
          // Move Transcription
          if (playerContainer.classList.contains("left-wrapper") || video_url_2) {
            transcriptWrapper.classList.remove("right-side");
          }
          while (transcriptWrapper.childNodes.length > 0) {
            transcriptWrapper2.appendChild(transcriptWrapper.childNodes[0]);
          }
        } else if (window.innerWidth <= 1024) {
          while (transcriptWrapper.childNodes.length > 0) {
            transcriptWrapper2.appendChild(transcriptWrapper.childNodes[0]);
          }
          transcriptWrapper.classList.remove("right-side");
        }
      } else {
        player.classList.remove("left-side");
        transcriptWrapper.classList.remove("right-side");
        // This is for two video players
        while (transcriptWrapper2.childNodes.length > 0) {
          transcriptWrapper.appendChild(transcriptWrapper2.childNodes[0]);
        }

        drawControls()
      }
      b_transcript = !b_transcript;
    }

    var drawControls = function () {
      if (!document.getElementsByClassName("all-container")[0]) {
        return;
      }
      document.getElementsByClassName("all-container")[0].style.opacity = 1; // This is for initial load
      player.style.height = player.offsetWidth * (740 / 1140) + 'px';
      if (transcription_url && load_transcript) {
        transcriptWrapper.style.height = player.offsetWidth * (740 / 1140) - 70 + 'px';
      }
      // transcript.style.height = player.offsetWidth * (740/1140)-70;
      if (video_url_2) {
        /*secondContainer.style.width = (playerContainer.parentElement.offsetWidth - playerContainer.offsetWidth - 5) + 'px'
        if (window.innerWidth > 1440) {
          secondContainer.style.width = (secondContainer.offsetWidth - 60) + 'px';
        }*/
        player2.style.height = player2.offsetWidth * (740 / 1140) + 'px';
      }
      /*if (window.innerWidth < 762) {
        let controlBar = document.getElementById("control-bar")
        if ((!controlBar.classList.contains("mob-1")) && (!controlBar.classList.contains("mob-2"))) {
          controlBar.classList.add("mob-1")
        }
      } else {
        let controlBar = document.getElementById("control-bar")
        controlBar.classList.remove("mob-1")
        controlBar.classList.remove("mob-2")
      }*/

      // POST Messge to parent frame
      let w = document.body.clientWidth
      let h = document.body.clientHeight
      window.parent.postMessage({ w: w, h: h }, "*");
    }

    window.onresize = function (event) {
      drawControls()
    };

    window.addEventListener("orientationchange", function () {
      drawControls()
    });

    drawControls()

    if (video_url_2) {
      /*setTimeout(function () {
        sideButton.click()
      }, 100)*/

      setTimeout(function () {
        if (transcription_url) {
          setUpCaption();
        }
        drawControls();
      }, 200);
    } else {
      if (transcription_url) {
        setUpCaption();
      }
    }
  }

  /*function removeControls() {
    var el = document.getElementsByClassName('all-container')[0];
    var elClone = el.cloneNode(true);
    el.parentNode.replaceChild(elClone, el);
  }*/

  /*function convertSeconds(totalSeconds) {
    if (!totalSeconds) {
      return 0;
    }
    var date = new Date(null);
    date.setSeconds(totalSeconds); // specify value for SECONDS here
    var result = date.toISOString().substr(11, 8);
    return result;
    // let hours = Math.floor(totalSeconds / 3600);
    // totalSeconds %= 3600;
    // let minutes = Math.floor(totalSeconds / 60);
    // let seconds = totalSeconds % 60;
    // return hours+":"+minutes+":"+Number(seconds).toFixed(0);
  }*/

  function loadCaptions() {
    var r = new XMLHttpRequest();
    r.onreadystatechange = function () {
      if (r.readyState == 4 && r.status == 200) {
        var t = r.responseText.split(/\n\s*\n/);
        t.shift();
        var h = "<p>";
        var s = 0;
        for (var i = 0; i < t.length; i++) {
          var c = parse(t[i]);
          c.text = c.text.replace(/&gt;&gt;/g, '<br>') // This is for cielo case
          if (c.text.indexOf('<br>') === 0) {
            c.text = c.text.substring(4);
          }
          //var isOdd = true;
          var classType = 'caption caption-odd';
          if (i % 2 == 0) {
            //isOdd = false;
            classType = 'caption caption-even';
          }
          //h += "<span id='caption" + i + "' class='" + classType + "'><span class='caption-dot'></span><span class='caption-time'>" + '<i class="fa fa-play-circle-o" aria-hidden="true"></i> ' + c.btext + "&nbsp&nbsp&nbsp&nbsp" + "</span><span class='caption-text'>" + c.text + "</span></span>";
          h += "<span id='caption" + i + "' class='" + classType + "'><i class='far fa-clock caption-clock'></i><span class='caption-time'>" + 
          //'<i class="fa fa-play-circle-o" aria-hidden="true"></i> ' + 
          c.btext + "&nbsp&nbsp&nbsp&nbsp" + "</span><span class='caption-text'>" + c.text + "</span></span>";
          captions.push(c);
        }
        transcript.innerHTML = h + "</p>";
        transcript.classList.add("active");
        heatmapWrapper.classList.add("active");
        load_transcript = true;
        loadHeatmap();
        if (keywords) {
          searchTranscript(keywords);
        } else {
          checkAllBlocks();
        }
      } else if (r.status == 404) {
        load_transcript = false;
      }
    };
    r.open('GET', transcription_url, true);
    r.send();
  };

  function loadHeatmap() {
    var ht = '';
    for (var i = 0; i < nodeSize; i++) {
      ht += "<div id='heatmap-block-" + i + "' class='heatmap-block'></div>";
    }
    heatmapBlocks.innerHTML = ht;
  }

  function parse(d) {
    var a = d.split("\n");
    var i = a[1].indexOf(' --> ');
    var t = a[2];
    if (a[3]) {
      t += " " + a[3];
    }
    t = t.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return {
      begin: seconds(a[1].substr(0, i)),
      btext: a[1].substr(3, i - 7),
      end: seconds(a[1].substr(i + 5)),
      text: t
    }
  };

  function seconds(s) {
    var a = s.split(':');
    var r = Number(a[a.length - 1]) + Number(a[a.length - 2]) * 60;
    if (a.length > 2) {
      r += Number(a[a.length - 3]) * 3600;
    }
    return r;
  };

  // Highlight current caption and chapter
  jwplayer1.on('time', function (e) {
    var p = e.position;
    for (var j = 0; j < captions.length; j++) {
      if (captions[j].begin < p && captions[j].end > p) {
        if (j != caption) {
          var c = document.getElementById('caption' + j);
          if (caption > -1) {
            //document.getElementById('caption' + caption).className = "";
            document.getElementById('caption' + caption).classList.remove('current');
          }
          c.classList.add("current");
          if (query == "") {
            transcript.scrollTop = c.offsetTop - transcript.offsetTop - 40;
          }
          caption = j;
        }
        break;
      }
    }
  });

  // Hook up interactivity
  if (transcript) {
    transcript.addEventListener("click", function (e) {
      if (e.target.id.indexOf("caption") == 0) {
        var i = Number(e.target.id.replace("caption", ""));
        jwplayer().seek(captions[i].begin);
      }
    });
  }

  function getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  // Execute search
  function searchTranscript(q) {
    if (query === q) {
      nextWordButton.click();
      return;
    }
    var colorTyle = getRandomInt(0,5);
    var colorClass = "tulip";
    switch(colorTyle) {
      case 1:
        colorClass = "pink";
        break;
      case 2:
        colorClass = "peach";
        break;
      case 3:
        colorClass = "tulip";
        break;
      case 4:
        colorClass = "purple";
        break;
      case 5:
        colorClass = "onahau";
        break;
      case 0:
      default:
        colorClass = "gossip";
    } 
    resetSearch();
    matches = [];
    query = q;
    for (var i = 0; i < captions.length; i++) {
      var m = captions[i].text.toLowerCase().indexOf(q.toLowerCase());
      if (m > -1) {
        if (document.getElementById('caption' + i)) {
          document.getElementById('caption' + i).innerHTML =
            //"<span class='caption-dot'></span><span class='caption-time'>" +
            "<i class='far fa-clock caption-clock'></i><span class='caption-time'>" + 
            //'<i class="fa fa-play-circle-o" aria-hidden="true"></i> ' +
            captions[i].btext + "&nbsp&nbsp&nbsp&nbsp" + "</span><span class='caption-text'>" +
            captions[i].text.substr(0, m) + "<em class='" + colorClass + "'>" +
            captions[i].text.substr(m, q.length) + "</em>" +
            captions[i].text.substr(m + q.length) + "</span>";
          matches.push(i);
        }
      }
    }
    /*if (q === '') {
      matches = [];
    }*/
    checkBlocks(matches);
    if (matches.length) {
      cycleSearch(0);
      searchInput.classList.remove('min');
      searchInput.classList.add('max');
      searchTotal.innerHTML = matches.length;
      countWordWrapper.classList.add('active');
      backWordButton.classList.add('active');
      nextWordButton.classList.add('active');
      closeWordButton.classList.add('active');
    } else {
      searchInput.classList.add('min');
      searchCurrent.innerHTML = "0";
      searchTotal.innerHTML = matches.length;
      countWordWrapper.classList.add('active');
      closeWordButton.classList.add('active');
      resetSearch();
    }
  };

  function cycleSearch(i) {
   //console.log("cycleSearch i, cycle", i, cycle);
   //console.log("cycleSearch matches", matches[i], matches[cycle], matches);
   //console.log("cycleSearch captions", captions);
    if (cycle > -1) {
      var old = document.getElementById('caption' + matches[cycle]);
      if (undefined != old) {
        if (old.getElementsByTagName("em").length > 0)
        old.getElementsByTagName("em")[0].className = "";
      }
    }
    jwplayer().seek(captions[matches[i]].begin);
    var c = document.getElementById('caption' + matches[i]);
    //c.getElementsByTagName("em")[0].className = "current";
    transcript.scrollTop = c.offsetTop - transcript.offsetTop - 40;
    cycle = i;
    searchCurrent.innerHTML = cycle + 1;
  };

  function resetSearch() {
    if (matches.length) {
      for (var i = 0; i < captions.length; i++) {
        document.getElementById('caption' + i).innerHTML = 
          //"<span class='caption-dot'></span><span class='caption-time'>" + 
          "<i class='far fa-clock caption-clock'></i><span class='caption-time'>" + 
          //'<i class="fa fa-play-circle-o" aria-hidden="true"></i> ' + 
          captions[i].btext + "&nbsp&nbsp&nbsp&nbsp" + "</span><span class='caption-text'>" + 
          captions[i].text + "</span>"
      }
    }
    query = "";
    matches = [];
    cycle = -1;
    transcript.scrollTop = 0;
    
    searchInput.classList.remove('max');
    //searchInput.classList.remove('min');
    //countWordWrapper.classList.remove('active');
    backWordButton.classList.remove('active');
    nextWordButton.classList.remove('active');
    //closeWordButton.classList.remove('active');
  };

  function resetBlocks() {
    blocks = [];
    for (let i = 0; i < nodeSize; i++) {
      let block = {};
      block.index = i;
      block.size = 0;
      block.currentMatche = 0;
      blocks.push(block);
    }
  }

  function checkBlocks(matches) {
    //console.log("checkBlocks matches", matches);
    resetBlocks();

    for (let j = 0; j < matches.length; j++) {
      getMatch(query, matches[j], j);
    }

    displayBlocks();
  }

  function getMatch(key, match, j) {
    //console.log("-------------------------------");
    //var mats;
    //var lastIndex = -1;
    let index = Math.floor(match*nodeSize/captions.length);
    /*if (lastIndex != index) {
      lastIndex = index;
      //mats = [];
    }*/
    let mat = {};
    mat.begin = captions[match].begin;
    mat.match = j;
    mat.query = key;
    if (undefined == blocks[index].matches) {
      blocks[index].matches = [];
    }
    blocks[index].matches.push(mat);
    blocks[index].size = blocks[index].size + 1;
    //console.log("get Match key::::", key, match, j, query, lastIndex, index);
    //console.log("get Match block:::", index, blocks[index]);   //captions[match]
    blocks[index].matches.sort(function (a, b) {
      return a.begin - b.begin;
    });
  }

  function checkAllBlocks() {
    resetBlocks();
    
    getAllMatches();
    
    /*for (let j = 0; j < matches.length; j++) {
      getMatch(query, matches[j], j);
    }*/
    let j = -1;
    for (var k = 0; k < queries.length; k++) {
      let key = queries[k];  //keyword
      //console.log("get All Matches", k, key);
      for (var i = 0; i < captions.length; i++) {
        var m = captions[i].text.toLowerCase().indexOf(key.toLowerCase());
        if (m > -1) {
          j++;
          getMatch(key, i, j);
        }
      }
    }
    //console.log("checkAllBlocks blocks", blocks);

    displayBlocks();
  }

  function getAllMatches() {
    matches = [];
    for (var k = 0; k < queries.length; k++) {
      let key = queries[k];  //keyword
      //console.log("get All Matches", k, key);
      for (var i = 0; i < captions.length; i++) {
        var m = captions[i].text.toLowerCase().indexOf(key.toLowerCase());
        if (m > -1) {
          matches.push(i);
        }
      }
    }
    //matches.sort((a, b) => a - b);
    //console.log("get All Matches matches", matches);
    //console.log("get All Matches queries", queries);
  }

  function displayBlocks() {
    //console.log("displayBlocks", blocks, nodeSize);
    for (let i = 0; i < nodeSize; i++) {
      let blockElement = document.getElementById('heatmap-block-' + i);
      let block = blocks[i];
      if (blocks[i].size > 0) {
        blockElement.classList.add('active');
        if (blocks[i].size > 2)
          blockElement.classList.add('much');
        else
          blockElement.classList.remove('much');
        //blockElement.title = query + "(" + block.size + ")";
        blockElement.title = getTypeOfMatches(block.matches);
        //console.log("displayBlocks i, block:::", i, block, block.matches);
        blockElement.onclick = function(){
          if (block.currentMatche < block.matches.length-1) {
            block.currentMatche = block.currentMatche + 1;
          } else {
            block.currentMatche = 0;
          }
          let mat = block.matches[block.currentMatche];
          jwplayer().seek(mat.begin);
          //console.log("displayBlocks i, block.mat", i, mat);
          //console.log("displayBlocks i, block, mat", i, block, mat);
          cycleSearch(mat.match);
        }
      } else {
        blockElement.classList.remove('active');
        blockElement.title = "No keyword";
        blockElement.onclick = function (event) {
          return false;
        }
      }
    }
  }

  function getTypeOfMatches(matches) {
    let type = "";
    for (var k = 0; k < queries.length; k++) {
      let key = queries[k];
      let size = 0;
      //console.log("getTypeOfMatches k key::", k, key);
      for (let i = 0; i < matches.length; i++) {
        let mat = matches[i];
        if (key == mat.query) {
          size++;
        }
      }
      if (size > 0) {
        //type += mat.query + "(" + mat.match + ") ";
        type += key + "(" + size + "), ";
      }
    }
    type = type.substring(0, type.length - 2);
    return type;
  }

  return {
    searchTranscript: searchTranscript
  }
}

function searchKeyword(keyword) {
  // var keyword = document.getElementById("keyword").value;
  // document.getElementById('search').value = keyword
  // document.getElementById("search").focus();
  Player.searchTranscript(keyword);
}

function startPlay(video_url_1, video_url_2, transcription_url, transcode_1, transcode_2, k, st_time) {
  k = k || null;
  st_time = st_time || null;
  if (k) {
    keywords = k;
  }
  if (st_time) {
    start_time = st_time;
  }
  document.getElementsByClassName("all-container")[0].style.opacity = 0;
  document.getElementById("initializing").style.display = "block";
  if (video_url_2) {
    document.getElementsByClassName("all-container")[0].classList.add("all-container-2")
  }
  ori_video_url_1 = video_url_1;
  ori_video_url_2 = video_url_2;
  Player = initJWPlayer(transcode_1 ? transcode_1 : video_url_1, transcode_2 ? transcode_2 : video_url_2, transcription_url);
  //CEUtility();
}

/*const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');
const currentTheme = localStorage.getItem('theme');

if (currentTheme) {
  document.documentElement.setAttribute('data-theme', currentTheme);

  if (currentTheme === 'dark' && toggleSwitch) {
    toggleSwitch.checked = true;
  }
}

function switchTheme(e) {
  if (e.target.checked) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
  }
}
if (toggleSwitch)toggleSwitch.addEventListener('change', switchTheme, false);*/