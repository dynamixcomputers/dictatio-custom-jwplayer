jwplayer.key = "rWNl23nkOEzswsr7LKlzCwYeTz8fLdjwPOL0CQoK9f0=";
let Player, keywords, start_time, ori_video_url_1, ori_video_url_2;

function initJWPlayer(video_url_1, video_url_2, transcription_url) {
  var chapters = [];
  var captions = [];
  var caption = -1;
  var matches = [];
  var query = "";
  var cycle = -1;

  var transcript = document.getElementById('transcript');
  var search = document.getElementById('search');
  var match = document.getElementById('match');
  var svgWrapper = document.getElementById("svg-wrapper");
  var keywordToggleButton = document.getElementById("toggleSearch");
  var show_keyword = true;
  var b_transcript = false;
  var load_transcript = false;
  var video, video1, seekBar, volumeBar, captionButton, player, player2, transcriptWrapper, keywordWrapper,
    playButton, muteButton, fullScreenButton, playSpeedButton, swapControlButton, resolutionControlButton, jw_player1, jw_player2, unmuteButtonWrapper;
  var transcriptWrapper2; // This is for multi videos
  // Two Video Player
  var playerContainer, secondContainer;
  // Two Video Players Control Button
  var sideButton, fullButton, swapButton;
  var wait2ndPlayerInerval, ready_1 = false, ready_2 = false;

  if (!document.getElementById("player")) {
    return;
  }
  if (video_url_1) {
    let id = Number(document.getElementById("meta-videoId").innerHTML);
    jwplayer("player").setup({
      file: video_url_1,
      displaytitle: false,
      width: 1140,
      height: 740,
      controls: false,
      mute: true,
      ga: {
        label: id
      }
    });
  }

  jwplayer("player").on('complete', function () {
    playButton.classList.add("Play");
  })

  if (video_url_2) {
    jwplayer("player2").setup({
      file: video_url_2,
      displaytitle: false,
      width: 1140,
      height: 740,
      controls: false,
      mute: true
    });
    jwplayer("player2").on('ready', function () {
      ready_2 = true;
      jwplayer("player2").play();
    })
  }

  jwplayer("player").on('ready', function () {
    ready_1 = true;
    jwplayer("player").play();
    wait2ndPlayerInerval = setInterval(function () {
      if ((ready_2 || !video_url_2) && ready_1) {
        if (transcription_url) {
          loadCaptions()
        } else if (!transcription_url) {
          let captionButton = document.getElementById("captionButton");
          captionButton.classList.add("no-caption")
        }
        setUpControls()
      }
    }, 10)
  })

  jwplayer("player").on("error", (err) => {
    try {
      // If transcription is not finished yet
      if ((Number(err.code) >= 230000) && (Number(err.code) < 240000)) {
        initJWPlayer(ori_video_url_1, ori_video_url_2, transcription_url)
      }
    } catch (e) {

    }
  })

  function setUpControls() {
    clearInterval(wait2ndPlayerInerval);
    video = document.getElementsByClassName("jw-video")[0];

    if (video_url_2) {
      // This is to sync both videos
      var b = setInterval(function () {
        if (!video) {
          video = document.getElementsByClassName("jw-video")[0];
        }
        if (video_url_2 && !video1) {
          video1 = document.getElementsByClassName("jw-video")[1];
        }
        if (jwplayer("player").getState() === 'playing' && jwplayer("player2").getState() === 'playing') {
          document.getElementById("initializing").style.display = "none";
          clearInterval(b);
          if (start_time) {
            let hh = Number(start_time.split(':')[0])
            let mm = Number(start_time.split(':')[1])
            let ss = Number(start_time.split(':')[2])
            jwplayer().seek(hh * 3600 + mm * 60 + ss);
            start_time = null;
          }
        } else if (jwplayer("player").getState() === 'playing' && jwplayer("player2").getState() === 'buffering') {
          jwplayer("player").pause()
        } else if (jwplayer("player").getState() === 'buffering' && jwplayer("player2").getState() === 'playing') {
          jwplayer("player2").pause()
        } else if (jwplayer("player").getState() === 'paused' && jwplayer("player2").getState() === 'paused') {
          if (document.getElementById("initializing")) {
            document.getElementById("initializing").style.display = "none";
          }
          jwplayer("player").play()
          jwplayer("player2").play()
          clearInterval(b);
          if (start_time) {
            let hh = Number(start_time.split(':')[0])
            let mm = Number(start_time.split(':')[1])
            let ss = Number(start_time.split(':')[2])
            jwplayer().seek(hh * 3600 + mm * 60 + ss);
            start_time = null;
          }
        } else if (jwplayer("player").getState() === 'paused' && jwplayer("player2").getState() === 'playing') {
          if (document.getElementById("initializing")) {
            document.getElementById("initializing").style.display = "none";
          }
          jwplayer("player").play()
          clearInterval(b);
          if (start_time) {
            let hh = Number(start_time.split(':')[0])
            let mm = Number(start_time.split(':')[1])
            let ss = Number(start_time.split(':')[2])
            jwplayer().seek(hh * 3600 + mm * 60 + ss);
            start_time = null;
          }
        } else if (jwplayer("player").getState() === 'playing' && jwplayer("player2").getState() === 'paused') {
          if (document.getElementById("initializing")) {
            document.getElementById("initializing").style.display = "none";
          }
          jwplayer("player2").play()
          clearInterval(b);
          if (start_time) {
            let hh = Number(start_time.split(':')[0])
            let mm = Number(start_time.split(':')[1])
            let ss = Number(start_time.split(':')[2])
            jwplayer().seek(hh * 3600 + mm * 60 + ss);
            start_time = null;
          }
        }
      }, 10);

      // This is for video mute
      var b_mute = false;
      var muteInterval = setInterval(function () {
        if (jwplayer("player").getState() === 'playing' && jwplayer("player2").getState() === 'playing' && !b_mute) {
          if (jwplayer("player").getMute()) {
            jwplayer("player").setMute(false)
            b_mute = true;
          }
        } else if (jwplayer("player").getState() === 'paused' && jwplayer("player2").getState() === 'playing' && b_mute) {
          jwplayer("player").setMute(true)
          jwplayer("player").play();
          if (start_time) {
            let hh = Number(start_time.split(':')[0])
            let mm = Number(start_time.split(':')[1])
            let ss = Number(start_time.split(':')[2])
            jwplayer().seek(hh * 3600 + mm * 60 + ss);
            start_time = null;
          }
          unmuteButtonWrapper.style.display = "block";
        } else if (jwplayer("player").getState() === 'playing' && jwplayer("player").getMute() === false) {
          unmuteButtonWrapper.style.display = "none";
          clearInterval(muteInterval);
        }
      }, 10);
    } else if (!video_url_2) {
      var b_mute = false;
      var muteInterval = setInterval(function () {
        if (jwplayer("player").getState() === 'playing' && !b_mute) {
          if (!document.getElementById("initializing")) {
            return;
          }
          document.getElementById("initializing").style.display = "none";
          if (jwplayer("player").getMute()) {
            jwplayer("player").setMute(false)
            b_mute = true;
          }
        } else if (jwplayer("player").getState() === 'paused' && b_mute) {
          // jwplayer("player").setMute(true)
          jwplayer("player").play();
          if (start_time) {
            let hh = Number(start_time.split(':')[0])
            let mm = Number(start_time.split(':')[1])
            let ss = Number(start_time.split(':')[2])
            jwplayer().seek(hh * 3600 + mm * 60 + ss);
            start_time = null;
          }
          unmuteButtonWrapper.style.display = "block";
        } else if (jwplayer("player").getState() === 'playing' && jwplayer("player").getMute() === false && b_mute) {
          unmuteButtonWrapper.style.display = "none";
          clearInterval(muteInterval);
          if (start_time) {
            let hh = Number(start_time.split(':')[0])
            let mm = Number(start_time.split(':')[1])
            let ss = Number(start_time.split(':')[2])
            jwplayer().seek(hh * 3600 + mm * 60 + ss);
            start_time = null;
          }
        }
      }, 10);
    }

    video1 = document.getElementsByClassName("jw-video")[1];
    if (video1) {
      video1.muted = true;
    }
    // Buttons
    playButton = document.getElementById("playPauseButton");
    nextWordButton = document.getElementById("nextWordButton");
    muteButton = document.getElementById("mute");
    fullScreenButton = document.getElementById("full-screen");
    swapControlButton = document.getElementById("swap-control");
    playSpeedButton = document.getElementById("playSpeedButton");
    resolutionControlButton = document.getElementById("resolution-control");

    // Sliders
    seekBar = document.getElementById("seek-bar");
    volumeBar = document.getElementById("volume-bar");

    // Caption Button
    captionButton = document.getElementById("captionButton");
    player = document.getElementById('player');
    player2 = document.getElementById('player2');

    // Unmute button for interactive document
    unmuteButtonWrapper = document.getElementById("unmuteButtonWrapper");

    transcriptWrapper = document.getElementsByClassName("transcript-wrapper")[0]
    keywordWrapper = document.getElementsByClassName("keyword-wrapper")[0]

    playerContainer = document.getElementsByClassName("player-container")[0]
    secondContainer = document.getElementsByClassName("second-container")[0]
    transcriptWrapper2 = document.getElementsByClassName("transcript-wrapper-2")[0]

    // Two Video Player
    if (video_url_2) {
      sideButton = document.getElementsByClassName("stage-sideBySideButton")[0]
      fullButton = document.getElementsByClassName("stage-fullFrameButton")[0]
      swapButton = document.getElementsByClassName("stage-swapButton")[0]

      sideButton.addEventListener("click", function () {
        player.classList.remove("left-side");
        // transcript.classList.remove("right-side");
        transcriptWrapper.classList.remove("right-side");
        // player.style.width = 1140;
        // player.style.height = 740;
        if (video_url_2) {
          playerContainer.classList.add("left-wrapper")
          secondContainer.classList.add('right-wrapper')
          secondContainer.style.width = (playerContainer.parentElement.offsetWidth - playerContainer.offsetWidth - 5)
          if (window.innerWidth > 1440) {
            secondContainer.style.width = secondContainer.offsetWidth - 60;
          }
          player2.style.height = player2.offsetWidth * (740 / 1140);

          //
          document.getElementsByClassName("all-container")[0].classList.add("two-video-container");
        }
        b_transcript = false

        sideButton.classList.add("active")
        fullButton.classList.remove("active")
        swapButton.classList.remove("active")
        drawControls()
      })
      fullButton.addEventListener("click", function () {
        playerContainer.classList.remove("left-wrapper")
        secondContainer.classList.remove("right-wrapper")
        fullButton.classList.add("active")
        sideButton.classList.remove("active")
        swapButton.classList.remove("active")

        //
        document.getElementsByClassName("all-container")[0].classList.remove("two-video-container");
        drawControls()
      })
      swapButton.addEventListener("click", function () {
        swapButton.classList.add("active")
        fullButton.classList.remove("active")
        sideButton.classList.remove("active")
        if (!video1) {
          return;
        }
        let video_parent = video.parentElement;
        let video1_parent = video1.parentElement;
        video_parent.appendChild(video1);
        video1_parent.appendChild(video);
        video.play();
        if (video1) {
          video1.play();
        }
      })
    } else {
      try {
        document.getElementsByClassName("stage-sideBySideButton")[0].classList.add("opacity-2")
        document.getElementsByClassName("stage-fullFrameButton")[0].classList.add("opacity-2")
        document.getElementsByClassName("stage-swapButton")[0].classList.add("opacity-2")
        document.getElementById("swap-control").classList.add("opacity-2")
      } catch (err) {
        console.log(err);
      }
    }
    // Event listener for the play/pause button
    playButton.addEventListener("click", function () {
      if (video.paused == true) {
        // Play the video
        video.play();
        if (video1) {
          video1.play();
        }
        // Update the button text to 'Pause'
        playButton.classList.add("Pause");
        playButton.classList.remove("Play");
      } else {
        // Pause the video
        video.pause();
        if (video1) {
          video1.pause();
        }
        // Update the button text to 'Play'
        playButton.classList.add("Play");
        playButton.classList.remove("Pause");
      }
    });

    // Event listener for the mute button
    muteButton.addEventListener("click", function () {
      if (video.muted == false) {
        // Mute the video
        video.muted = true;

        // Update the button text
        muteButton.classList.add('in-active')
        muteButton.classList.remove('active')
      } else {
        // Unmute the video
        video.muted = false;

        // Update the button text
        muteButton.classList.add('active')
        muteButton.classList.remove('in-active')
      }

      if (video1 && video1.muted == false) {
        // Mute the video
        video1.muted = true;

        // Update the button text
        muteButton.classList.add('in-active')
        muteButton.classList.remove('active')
      } else if (video1 && video1.muted === true) {
        // Unmute the video
        video1.muted = true;

        // Update the button text
        muteButton.classList.add('active')
        muteButton.classList.remove('in-active')
      }
    });

    // Event listener for the full-screen button
    fullScreenButton.addEventListener("click", function () {
      if (video.requestFullscreen) {
        video.requestFullscreen();
      } else if (video.mozRequestFullScreen) {
        video.mozRequestFullScreen(); // Firefox
      } else if (video.webkitRequestFullscreen) {
        video.webkitRequestFullscreen(); // Chrome and Safari
      }
    });

    // Event lister for swap control button on mobile
    swapControlButton.addEventListener("click", function () {
      let controlBar = document.getElementById("control-bar")
      if (controlBar.classList.contains("mob-1")) {
        controlBar.classList.remove("mob-1")
        controlBar.classList.add("mob-2")
      } else if (controlBar.classList.contains("mob-2")) {
        controlBar.classList.remove("mob-2")
        controlBar.classList.add("mob-1")
      }
    })

    // Event listener for play speed button
    playSpeedButton.addEventListener("click", function () {
      video.playbackRate = video.playbackRate + 0.5;
      if (video.playbackRate === 2.5) {
        video.playbackRate = 1;
      }
      if (video1) {
        video1.playbackRate = video.playbackRate + 0.5;
        if (video1.playbackRate === 2.5) {
          video1.playbackRate = 1;
        }
      }

      playSpeedButton.innerHTML = video.playbackRate + 'x';
    })

    nextWordButton.addEventListener("click", function () {
      if (matches.length === 0) return;
      if (cycle >= matches.length - 1) {
        cycleSearch(0);
      } else {
        cycleSearch(cycle + 1);
      }
    })

    resolutionControlButton.addEventListener("click", function () {
      if (document.getElementById("resolution-items").style.display !== 'block') {
        document.getElementById("resolution-items").style.display = "block";
      } else {
        document.getElementById("resolution-items").style.display = "none";
      }
    });

    unmuteButtonWrapper.addEventListener("click", function () {
      jwplayer("player").setMute(false)
      unmuteButtonWrapper.style.display = "none";
    });

    (function () {
      let b_auto = document.getElementsByClassName("auto")[0]
      let b_360p = document.getElementsByClassName("360p")[0]
      let b_480p = document.getElementsByClassName("480p")[0]
      let b_720p = document.getElementsByClassName("720p")[0]
      let b_1080p = document.getElementsByClassName("1080p")[0]

      b_auto.addEventListener("click", function () {
        swapVideo('auto');
        document.getElementById("resolution-items").style.display = "none";
        b_auto.classList.add("active");
        b_360p.classList.remove("active");
        b_480p.classList.remove("active");
        b_720p.classList.remove("active");
        b_1080p.classList.remove("active");
      })
      b_360p.addEventListener("click", function () {
        swapVideo(360);
        document.getElementById("resolution-items").style.display = "none";
        b_auto.classList.remove("active");
        b_360p.classList.add("active");
        b_480p.classList.remove("active");
        b_720p.classList.remove("active");
        b_1080p.classList.remove("active");
      })
      b_480p.addEventListener("click", function () {
        swapVideo(480);
        document.getElementById("resolution-items").style.display = "none";
        b_auto.classList.remove("active");
        b_480p.classList.add("active");
        b_360p.classList.remove("active");
        b_720p.classList.remove("active");
        b_1080p.classList.remove("active");
      })
      b_720p.addEventListener("click", function () {
        swapVideo(720);
        document.getElementById("resolution-items").style.display = "none";
        b_auto.classList.remove("active");
        b_720p.classList.add("active");
        b_480p.classList.remove("active");
        b_360p.classList.remove("active");
        b_1080p.classList.remove("active");
      })
      b_1080p.addEventListener("click", function () {
        swapVideo(1080);
        document.getElementById("resolution-items").style.display = "none";
        b_auto.classList.remove("active");
        b_1080p.classList.add("active");
        b_480p.classList.remove("active");
        b_720p.classList.remove("active");
        b_360p.classList.remove("active");
      })
    })();

    let seekBar_move = true;
    // Event listener for the seek bar
    seekBar.addEventListener("change", function () {
      // Calculate the new time
      var time = video.duration * (seekBar.value / 1);
      // Update the video time
      video.currentTime = time;
      if (video1) {
        video1.currentTime = time;
      }
      seekBar_move = true;
      video.play();
      if (video1) {
        video1.play();
      }
    });

    seekBar.addEventListener('mousedown', function () {
      seekBar_move = false;
    })

    setInterval(function () {
      // Calculate the slider value
      if (!video) {
        video = document.getElementsByClassName("jw-video")[0];
        return;
      }
      var value = (1 / video.duration) * video.currentTime;
      // Update the slider value
      if (value && seekBar_move) {
        seekBar.value = value
      }
      if (document.getElementsByClassName("end-time")[0] && (convertSeconds(video.duration) !== 0)) {
        if (convertSeconds(video.duration) !== '00:00:00') {
          document.getElementsByClassName("end-time")[0].innerHTML = convertSeconds(video.duration);
        } else {
          document.getElementsByClassName("end-time")[0].innerHTML = '00:00:00'
        }
        if (convertSeconds(video.currentTime !== '00:00:00')) {
          document.getElementsByClassName("current-time")[0].innerHTML = convertSeconds(video.currentTime);
        } else {
          document.getElementsByClassName("current-time")[0].innerHTML = '00:00:00'
        }
      }
    }, 500)

    setInterval(function () {
      try {
        if (!video1) {
          return;
        }
        if (video1.currentTime > video.currentTime + 0.2 || video1.currentTime < video.currentTime - 0.2) {
          video1.currentTime = video.currentTime;
        }
        if (!video.paused && video1.paused) {
          video1.play();
        }
      } catch (err) {
        console.log(err);
      }
    }, 200)

    // Event listener for the volume bar
    volumeBar.addEventListener("change", function () {
      // Update the video volume
      video.volume = volumeBar.value;
      if (video1) {
        video1.volume = volumeBar.value;
      }
    });

    //Event for caption button
    captionButton.addEventListener("click", function () {
      if (!transcription_url) {
        captionButton.classList.add("no-caption")
        return;
      }
      if (!b_transcript) {
        player.classList.add("left-side");

        // This is for two video players
        while (transcriptWrapper2.childNodes.length > 0) {
          transcriptWrapper.appendChild(transcriptWrapper2.childNodes[0]);
        }

        transcriptWrapper.classList.add("right-side");

        // Multi Video
        if (video_url_2) {
          sideButton.classList.remove("active")
          fullButton.classList.remove("active")
          swapButton.classList.remove("active")

          // Move Transcription
          if (playerContainer.classList.contains("left-wrapper") || video_url_2) {
            transcriptWrapper.classList.remove("right-side");
          }
          while (transcriptWrapper.childNodes.length > 0) {
            transcriptWrapper2.appendChild(transcriptWrapper.childNodes[0]);
          }
        } else if (window.innerWidth <= 1024) {
          while (transcriptWrapper.childNodes.length > 0) {
            transcriptWrapper2.appendChild(transcriptWrapper.childNodes[0]);
          }
          transcriptWrapper.classList.remove("right-side");
        }
      } else {
        player.classList.remove("left-side");
        transcriptWrapper.classList.remove("right-side");
        // This is for two video players
        while (transcriptWrapper2.childNodes.length > 0) {
          transcriptWrapper.appendChild(transcriptWrapper2.childNodes[0]);
        }

        drawControls()
      }
      b_transcript = !b_transcript;
    })

    keywordToggleButton.addEventListener("click", function () {
      var keywordWrapper = document.getElementsByClassName("keyword-wrapper")[0];
      if (show_keyword) {
        keywordWrapper.classList.remove("hide");
        keywordToggleButton.classList.add('active')
        keywordWrapper.style.width = document.getElementsByClassName("transcript-wrapper")[0].offsetWidth;
      } else {
        keywordWrapper.classList.add("hide");
        keywordToggleButton.classList.remove('active')
        searchKeyword('')
      }
      show_keyword = !show_keyword;
    })

    var drawControls = function () {
      if (!document.getElementsByClassName("all-container")[0]) {
        return;
      }
      document.getElementsByClassName("all-container")[0].style.opacity = 1; // This is for initial load
      player.style.height = player.offsetWidth * (740 / 1140) + 'px';
      if (transcription_url && load_transcript) {
        transcriptWrapper.style.height = player.offsetWidth * (740 / 1140) - 70 + 'px';
      }
      // transcript.style.height = player.offsetWidth * (740/1140)-70;
      if (video_url_2) {
        secondContainer.style.width = (playerContainer.parentElement.offsetWidth - playerContainer.offsetWidth - 5) + 'px'
        if (window.innerWidth > 1440) {
          secondContainer.style.width = (secondContainer.offsetWidth - 60) + 'px';
        }
        player2.style.height = player2.offsetWidth * (740 / 1140) + 'px';
      }
      if (window.innerWidth < 762) {
        let controlBar = document.getElementById("control-bar")
        if ((!controlBar.classList.contains("mob-1")) && (!controlBar.classList.contains("mob-2"))) {
          controlBar.classList.add("mob-1")
        }
      } else {
        let controlBar = document.getElementById("control-bar")
        controlBar.classList.remove("mob-1")
        controlBar.classList.remove("mob-2")
      }

      // POST Messge to parent frame
      let w = document.body.clientWidth
      let h = document.body.clientHeight
      window.parent.postMessage({ w: w, h: h }, "*");
    }

    window.onresize = function (event) {
      drawControls()
    };

    window.addEventListener("orientationchange", function () {
      drawControls()
    });

    drawControls()

    if (video_url_2) {
      setTimeout(function () {
        sideButton.click()
      }, 100)

      setTimeout(function () {
        if (transcription_url) {
          captionButton.click()
        }
        drawControls()
      }, 200)
    } else {
      if (transcription_url) {
        captionButton.click()
      }
    }

  }

  function removeControls() {
    var el = document.getElementsByClassName('all-container')[0];
    var elClone = el.cloneNode(true);
    el.parentNode.replaceChild(elClone, el);
  }

  function convertSeconds(totalSeconds) {
    if (!totalSeconds) {
      return 0;
    }
    var date = new Date(null);
    date.setSeconds(totalSeconds); // specify value for SECONDS here
    var result = date.toISOString().substr(11, 8);
    return result;
    // let hours = Math.floor(totalSeconds / 3600);
    // totalSeconds %= 3600;
    // let minutes = Math.floor(totalSeconds / 60);
    // let seconds = totalSeconds % 60;
    // return hours+":"+minutes+":"+Number(seconds).toFixed(0);
  }

  function loadCaptions() {
    var r = new XMLHttpRequest();
    r.onreadystatechange = function () {
      if (r.readyState == 4 && r.status == 200) {
        var t = r.responseText.split(/\n\s*\n/);
        t.shift();
        var h = "<p>";
        var s = 0;
        for (var i = 0; i < t.length; i++) {
          var c = parse(t[i]);
          c.text = c.text.replace(/&gt;&gt;/g, '<br>') // This is for cielo case
          if (c.text.indexOf('<br>') === 0) {
            c.text = c.text.substring(4);
          }
          h += "<span id='caption" + i + "' class='caption'><span class='caption-dot'></span><span class='caption-time'>" + '<i class="fa fa-play-circle-o" aria-hidden="true"></i> ' + c.btext + "&nbsp&nbsp&nbsp&nbsp" + "</span><span class='caption-text'>" + c.text + "</span></span>";
          captions.push(c);
        }
        transcript.innerHTML = h + "</p>";
        load_transcript = true;
        if (keywords) {
          searchTranscript(keywords);
        }
      } else if (r.status == 404) {
        load_transcript = false;
      }
    };
    r.open('GET', transcription_url, true);
    r.send();
  };

  function parse(d) {
    var a = d.split("\n");
    var i = a[1].indexOf(' --> ');
    var t = a[2];
    if (a[3]) {
      t += " " + a[3];
    }
    t = t.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return {
      begin: seconds(a[1].substr(0, i)),
      btext: a[1].substr(3, i - 7),
      end: seconds(a[1].substr(i + 5)),
      text: t
    }
  };

  function seconds(s) {
    var a = s.split(':');
    var r = Number(a[a.length - 1]) + Number(a[a.length - 2]) * 60;
    if (a.length > 2) {
      r += Number(a[a.length - 3]) * 3600;
    }
    return r;
  };

  function swapVideo(quality) {
    // removeControls();
    if (quality === 360) {
      jwplayer().setCurrentQuality(4);
    } else if (quality === 480) {
      jwplayer().setCurrentQuality(3);
    } else if (quality === 720) {
      jwplayer().setCurrentQuality(2);
    } else if (quality === 1080) {
      jwplayer().setCurrentQuality(1);
    } else if (quality == 'auto') {
      jwplayer().setCurrentQuality(0);
    }
  }

  // Highlight current caption and chapter
  jwplayer().on('time', function (e) {
    var p = e.position;
    for (var j = 0; j < captions.length; j++) {
      if (captions[j].begin < p && captions[j].end > p) {
        if (j != caption) {
          var c = document.getElementById('caption' + j);
          if (caption > -1) {
            document.getElementById('caption' + caption).className = "";
          }
          c.className = "current";
          if (query == "") {
            transcript.scrollTop = c.offsetTop - transcript.offsetTop - 40;
          }
          caption = j;
        }
        break;
      }
    }
  });

  // Hook up interactivity
  if (transcript) {
    transcript.addEventListener("click", function (e) {
      if (e.target.id.indexOf("caption") == 0) {
        var i = Number(e.target.id.replace("caption", ""));
        jwplayer().seek(captions[i].begin);
      }
    });
  }
  if (search) {
    search.addEventListener('focus', function (e) {
      setTimeout(function () {
        search.select();
      }, 100);
    });
    search.addEventListener('keydown', function (e) {
      if (e.keyCode == 27) {
        resetSearch();
      } else if (e.keyCode == 13) {
        var q = this.value.toLowerCase();
        if (q.length > 0) {
          if (q == query) {
            if (cycle >= matches.length - 1) {
              cycleSearch(0);
            } else {
              cycleSearch(cycle + 1);
            }
          } else {
            searchTranscript(q);
          }
        } else {
          resetSearch();
        }
      }
    });
  }

  // Execute search
  function searchTranscript(q) {
    if (query === q) {
      nextWordButton.click();
      return;
    }
    resetSearch();
    matches = [];
    query = q;
    for (var i = 0; i < captions.length; i++) {
      var m = captions[i].text.toLowerCase().indexOf(q.toLowerCase());
      if (m > -1) {
        if (document.getElementById('caption' + i)) {
          document.getElementById('caption' + i).innerHTML =
            "<span class='caption-dot'></span><span class='caption-time'>" + '<i class="fa fa-play-circle-o" aria-hidden="true"></i> ' +
            captions[i].btext + "&nbsp&nbsp&nbsp&nbsp" + "</span><span class='caption-text'>" +
            captions[i].text.substr(0, m) + "<em>" +
            captions[i].text.substr(m, q.length) + "</em>" +
            captions[i].text.substr(m + q.length) + "</span>";
          matches.push(i);
        }
      }
    }
    if (q === '') {
      matches = [];
    }
    displaysvg(matches);
    if (matches.length) {
      cycleSearch(0);
      nextWordButton.classList.add('active');
    } else {
      nextWordButton.classList.remove('active');
      resetSearch();
    }
  };

  function cycleSearch(i) {
    if (cycle > -1) {
      var o = document.getElementById('caption' + matches[cycle]);
      o.getElementsByTagName("em")[0].className = "";
    }
    var c = document.getElementById('caption' + matches[i]);
    jwplayer().seek(captions[matches[i]].begin);
    // c.getElementsByTagName("em")[0].className = "current";
    // match.innerHTML = (i + 1) + " of " + matches.length;
    transcript.scrollTop = c.offsetTop - transcript.offsetTop - 40;
    cycle = i;
  };

  function resetSearch() {
    if (matches.length) {
      for (var i = 0; i < captions.length; i++) {
        document.getElementById('caption' + i).innerHTML = "<span class='caption-dot'></span><span class='caption-time'>" + '<i class="fa fa-play-circle-o" aria-hidden="true"></i> ' + captions[i].btext + "&nbsp&nbsp&nbsp&nbsp" + "</span><span class='caption-text'>" + captions[i].text + "</span>"
      }
    }
    query = "";
    matches = [];
    // match.innerHTML = "0 of 0";
    cycle = -1;
    transcript.scrollTop = 0;
  };

  function displaysvg(matches) {
    // [24, 25, 43, 44, 46, 47, 50, 59]
    svgWrapper.innerHTML = '';
    var ht = '';
    for (let i = 0; i < captions.length; i++) {
      for (let j = 0; j < matches.length; j++) {
        if (i === matches[j]) {
          ht +=
            '<div class="svg" style="left: ' + captions[i].begin / video.duration * 100 + '%" title="${captions[i].btext}" data-index="${i}">' +
            '<svg width="20px" height="20px" viewBox="0 0 400 400" xmlns="http://www.w3.org/2000/svg">' +
            '<path ' +
            'd="M 100 100 L 300 100 L 200 300 z"' +
            'fill="LightBlue"' +
            'stroke="Blue"' +
            'stroke-width="3"' +
            '/>' +
            '</svg>' +
            '</div>'
            ;
        }
      }
    }
    svgWrapper.innerHTML = ht;

    // SVG click event
    $(".svg").on('click', function () {
      for (let i = 0; i < captions.length; i++) {
        if (i === $(this).data('index')) {
          video.currentTime = captions[i].begin;
          if (video1) {
            video1.currentTime = captions[i].begin;
          }
          if (video.paused == true) {
            // Play the video
            video.play();
            if (video1) {
              video1.play();
              video1.muted = true;
            }
            // Update the button text to 'Pause'
            playButton.classList.add("Pause");
            playButton.classList.remove("Play");
          }
        }
      }
    })
  }

  return {
    searchTranscript: searchTranscript
  }
}

function searchKeyword(keyword) {
  // var keyword = document.getElementById("keyword").value;
  // document.getElementById('search').value = keyword
  // document.getElementById("search").focus();
  Player.searchTranscript(keyword);
}

function detectmob() {
  if (navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
  ) {
    return true;
  }
  else {
    return false;
  }
}

function startPlay(video_url_1, video_url_2, transcription_url, transcode_1, transcode_2, k, st_time) {
  k = k || null;
  st_time = st_time || null;
  document.getElementsByClassName("all-container")[0].style.opacity = 0;
  document.getElementById("initializing").style.display = "block";
  if (video_url_2) {
    document.getElementsByClassName("all-container")[0].classList.add("all-container-2")
  }
  ori_video_url_1 = video_url_1;
  ori_video_url_2 = video_url_2;
  Player = initJWPlayer(transcode_1 ? transcode_1 : video_url_1, transcode_2 ? transcode_2 : video_url_2, transcription_url);

  if (k) {
    keywords = k;
  }
  if (st_time) {
    start_time = st_time;
  }

  CEUtility();
}

function CEUtility() {
  let intervalDur = setInterval(function () {
    if (document.getElementById("player") && jwplayer("player")) {
      try {
        let video = document.getElementsByClassName("jw-video")[0];
        let videoId = Number(document.getElementById("meta-videoId").innerHTML);
        let duration = jwplayer("player").getDuration();
        var data = {}
        if (videoId && video && duration) {
          data.duration = duration;
          data.videoId = videoId;
          var xhr = new XMLHttpRequest();
          xhr.open('POST', '/api/credit/duration', true);
          xhr.onload = function () {
            // do something to response
            clearInterval(intervalDur);
          };
          xhr.setRequestHeader('Content-Type', 'application/json');
          xhr.send(JSON.stringify(data));
        }
      } catch (err) {
        console.log(err);
      }
    }
  }, 5000)
}

function startYoutube(url) {
  startPlay(url);
}