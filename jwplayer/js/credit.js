(function () {
    let deviceId;
    new Fingerprint2().get(function (result) {
        deviceId = result;
    });
    setInterval(function () {
        if (document.getElementById("player") && jwplayer("player") && jwplayer("player").getState() === 'playing') {
            try {
                let video = document.getElementsByClassName("jw-video")[0];
                let playRate = video.playbackRate;
                let userId = document.getElementById("meta-userId").innerHTML;
                let videoId = Number(document.getElementById("meta-videoId").innerHTML);
                var data = {};
                data.deviceId = deviceId;
                data.time = playRate * 5;
                if (userId) {
                    data.userId = userId;
                }
                if (!videoId) {
                    return;
                }
                data.videoId = videoId;
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/api/credit/watch', true);
                xhr.onload = function () {
                    // do something to response
                };
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(JSON.stringify(data));
            } catch (err) {
                console.log(err);
            }
        }
    }, 5000)
})()
